<?php

/*
 * Titan Framework options sample code. We've placed here some
 * working examples to get your feet wet
 * @see	http://www.titanframework.net/get-started/
 */


add_action( 'tf_create_options', 'kaffe_create_options' );

/**
 * Initialize Titan & options here
 */
function kaffe_create_options() {

	$titan = TitanFramework::getInstance( 'kaffe' );


	$titan->createCSS( '


		.intro h1,h5 {
			color: $header_color;
		}
		.single-top a {
			color: $header_color;
		}
		.single-player h1,51 {
			color: $header_color;
		}

		.kaffe-colors {
			background-color: $kaffe_color !important;
		}

		.pod-image img {
			border-color: $kaffe_color !important;
		}

		.overlay {
			background-color: $kaffe_color !important;
		}

		.button_container span {
			background-color: $kaffe_color;
		}

		::-webkit-scrollbar-thumb {
			background-color: $kaffe_color;
		}

		.hvr-underline-from-center:before {
			background: $kaffe_color;
		}

		#index-banner {
			background: $kaffe_header_img no-repeat center center fixed;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
		}

		.om-content {
			background-color: $header_bg_color;
		}

		.form-control, .form-group .form-control {
			border: 0;
			background-image: -webkit-gradient(linear, left top, left bottom, from($kaffe_color), to($kaffe_color)), -webkit-gradient(linear, left top, left bottom, from(#D2D2D2), to(#D2D2D2)) !important;
			background-image: -webkit-linear-gradient($kaffe_color, $kaffe_color), -webkit-linear-gradient(#D2D2D2, #D2D2D2) !important;
			background-image: -o-linear-gradient($kaffe_color, $kaffe_color), -o-linear-gradient(#D2D2D2, #D2D2D2)!important ;
			 background-image: linear-gradient($kaffe_color, $kaffe_color), linear-gradient(#D2D2D2, #D2D2D2) !important;
			-webkit-background-size: 0 2px, 100% 1px;
			background-size: 0 2px, 100% 1px;
			background-repeat: no-repeat;
			background-position: center bottom, center -webkit-calc(100% - 1px);
			background-position: center bottom, center calc(100% - 1px);
			background-color: rgba(0, 0, 0, 0);
			-webkit-transition: background 0s ease-out;
			-o-transition: background 0s ease-out;
			transition: background 0s ease-out;
			float: none;
			-webkit-box-shadow: none;
			box-shadow: none;
			border-radius: 0;
		 }


		.form-group.is-focused label, .form-group.is-focused label.control-label {
			color: $kaffe_color !important;
		}
		

		.btn, .btn-large {
    		background-color: $kaffe_color !important;
		}

		.kaffe-news-load-more {
			background-color: $kaffe_color;
		}

	' );


	/**
	 * Create an admin panel & tabs
	 * You should put options here that do not change the look of your theme
	 */

	$adminPanel = $titan->createAdminPanel( array(
	    'name' => __( 'Kaffe Settings', 'kaffe' ),
	) );

	/* GENERAL OPTIONS */

	$generalTab = $adminPanel->createTab( array(
	    'name' => __( 'General', 'kaffe' ),
	) );

	$generalTab->createOption( array(
		'name' => 'Overall Colors',
		'id' => 'kaffe_color',
		'type' => 'color',
		'default' => '#F44336',

	) );

	$generalTab->createOption( array(
		'name' => __( 'Font', 'kaffe' ),
		'id' => 'headings_font',
		'type' => 'font',
		'desc' => __( 'Select the font for the site', 'kaffe' ),
		'show_color' => false,
		'show_font_size' => false,
		'show_font_weight' => false,
		'show_font_style' => false,
		'show_line_height' => false,
		'show_letter_spacing' => false,
		'show_text_transform' => false,
		'show_font_variant' => false,
		'show_text_shadow' => false,
	    'default' => array(
	        'font-family' => 'Roboto, sans-serif',
	    ),
		'css' => 'h1, h2, h3, h4, h5, h6, p { value }',
	) );

	$generalTab->createOption( array(
	    'name' => __( 'Custom Css Code', 'kaffe' ),
	    'id' => 'custom_css',
	    'type' => 'code',
	    'desc' => 'Put your additional CSS rules here',
	    'lang' => 'css',
	) );

	$generalTab->createOption( array(
	    'name' => __( 'Custom Javascript Code', 'kaffe' ),
	    'id' => 'custom_js',
	    'type' => 'code',
	    'desc' => __( 'If you want to add some additional Javascript code into your site, add them here and it will be included in the frontend header. No need to add <code>script</code> tags', 'kaffe' ),
	    'lang' => 'javascript',
	) );

	$generalTab->createOption( array(
	    'type' => 'save',
	) );


	$headerTab = $adminPanel->createTab( array(
		 'name' => __( 'Header', 'kaffe' ),
	) );

/* HEADER OPTIONS */

	$headerTab->createOption( array(
	 'name' => 'Background Image',
	 'id' => 'kaffe_header_img',
	 'type' => 'upload',
	 'desc' => 'Upload your background image here'
	) );

	$headerTab->createOption( array(
		'name' => 'Header Color',
		'id' => 'header_color',
		'type' => 'color',
		'default' => 'rgba(0, 0, 0, 0.87);',

	) );


	$headerTab->createOption( array(
	'name' => 'Header Text',
	'id' => 'kaffe_header_text',
	'type' => 'text',
	'default' => 'Header Text',
	'desc' => 'Your Header Text'
	) );

	$headerTab->createOption( array(
	 'name' => 'Logo',
	 'id' => 'kaffe_logo_img',
	 'type' => 'upload',
	 'desc' => 'Upload your logo here'
	) );

	$headerTab->createOption( array(
	'name' => 'Sub Text',
	'id' => 'kaffe_sub_text',
	'type' => 'text',
	'default' => 'Sub Text',
	'desc' => 'Your Sub Text'
	) );

	$headerTab->createOption( array(
	'name' => 'Button Text',
	'id' => 'kaffe_header_button_text',
	'type' => 'text',
	'default' => 'Contact us',
	'desc' => 'To hide button leave this empty'
	) );

	$headerTab->createOption( array(
	'name' => 'My Select Page Option',
	'id' => 'kaffe_header_button_page',
	'type' => 'text',
	'desc' => 'e.g. http://rwikingsson.se/'
	) );

	$headerTab->createOption( array(
		 'type' => 'save',
	) );

/* INFO OPTIONS */

	$aboutTab = $adminPanel->createTab( array(
		 'name' => __( 'About', 'kaffe' ),
	) );

	$aboutTab->createOption( array(
		'name' => 'Header Background Color',
		'id' => 'header_bg_color',
		'type' => 'color',
		'default' => '#383732',

	) );

	$aboutTab->createOption( array(
	'name' => 'Header Text',
	'id' => 'kaffe_info_header_text',
	'type' => 'text',
	'default' => 'This is a Header',
	'desc' => 'Your Header Text'
	) );

	$aboutTab->createOption( array(
    'name' => __('My Info Text', 'kaffe' ),
    'id' => 'kaffe_info_text',
    'type' => 'editor',
	 'default' => 'Maecenas nec odio et ante tincidunt tempus. Nulla facilisi. Fusce pharetra convallis urna. Maecenas vestibulum mollis diam.
	 Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Praesent ac massa at ligula laoreet iaculis.. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui.',
    'desc' => 'Put your information content here'
	) );

	$aboutTab->createOption( array(
    'name' => 'My Upload Option',
    'id' => 'kaffe_info_img',
    'type' => 'upload',
    'desc' => 'Upload your image'
	) );

	$aboutTab->createOption( array(
	'name' => 'Button Text',
	'id' => 'kaffe_info_button_text',
	'type' => 'text',
	'default' => 'Contact us',
	'desc' => 'To hide button leave this empty'
	) );

	$aboutTab->createOption( array(
		 'type' => 'save',
	) );


	$footerTab = $adminPanel->createTab( array(
		 'name' => __( 'Footer', 'kaffe' ),
	) );

	$footerTab->createOption( array(
		'name' => __( 'Copyright Text', 'kaffe' ),
		'id' => 'copyright',
		'type' => 'text',
		'desc' => __( 'Enter your copyright text here (sample only)', 'kaffe' ),
	) );

	$footerTab->createOption( array(
		 'type' => 'save',
	) );


}
