<?php
/**
 * Kaffe functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Kaffe
 */


if ( ! function_exists( 'kaffe_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kaffe_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Kaffe, use a find and replace
	 * to change 'kaffe' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'kaffe', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	// Add thumb sizes below
	add_image_size( 'rectangle-thumb', 400, 350);
	add_image_size( 'large', 1200 , 720, true);
	add_image_size( 'small', 400, 400, true);
	add_image_size( 'circle', 500, 500, true );


	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'kaffe' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	/**
 * Add HTML5 theme support.
 */


	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'kaffe_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'kaffe_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kaffe_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kaffe_content_width', 640 );
}
add_action( 'after_setup_theme', 'kaffe_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kaffe_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'kaffe' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'kaffe_widgets_init' );



function kaffe_widgets_about() {
	register_sidebar( array(
		'name'          => esc_html__( 'Om oss', 'kaffe' ),
		'id'            => 'om-oss',
		'description'   => '',
		'before_widget' => '<div class="col s12 m12 card-panel om-content grey darken-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'kaffe_widgets_about' );


function my_project() {
    register_post_type( 'my-project',
        array(
            'labels' => array(
                'name' => __( 'My Projects' ),
                'singular_name' => __( 'My Project' )
            ),
            'public' => true,
            'has_archive' =>true,
            'rewrite' => array( 'slug' => 'my-project' ),
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'taxonomies' => array( 'post_tag' )
        )
    );
}
add_action( 'init', 'my_project' );


function wpdocs_custom_excerpt_length( $length ) {
    return 100	;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


add_action('wp_ajax_kaffe_get_more_news', 'kaffe_get_more_news');
add_action('wp_ajax_nopriv_kaffe_get_more_news', 'kaffe_get_more_news');

function kaffe_get_more_news() {
	$offset = $_REQUEST['offset'];
	$count = $_REQUEST['count'];
	$category = $_REQUEST['category'];
	$posttype = $_REQUEST['posttype'];
	//echo("<pre>".print_r($_REQUEST, 1)."</pre>");
	kaffe_get_news($offset, $count, $category, $posttype);

	die();
}
function kaffe_get_news($offset, $count, $category = 0, $post_type = 'post') {
	$array = array('posts_per_page' => $count, 'offset' => $offset, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => $post_type, 'post_status' => 'publish');
	if ($category != 0) $array['category'] = $category;

	$posts = get_posts($array);

	foreach ($posts as $i => $post) {
		$post_link = get_permalink($post->ID);
		$img = get_the_post_thumbnail($post->ID, '', array('class' => 'group list-group-image'));
		echo "<div class='item col-xs-12 col-sm-6 col-lg-4'>";
		echo "<div class='thumbnail'>";
			echo "$img ";
			echo "<div class='caption'>";
				echo "<h2 class='group inner list-group-item-heading'> $post->post_title </h2>";
				echo "<p class='group inner list-group-item-text'>".wp_trim_words($post->post_content, 20, "...")."</p>";
				echo "<a class='avsnittsguide hvr-underline-from-center' href='$post_link'>Lyssna på Avsnittet</a>";
			echo "</div>";
		echo "</div>";
		echo "</div>";
	}

}

function SearchFilter($query) {
  if ($query->is_search) {
    $query->set('cat','8');
  }
  return $query;
}
if(!is_admin())
  add_filter('pre_get_posts','SearchFilter');



/**
 * Generate custom search form
 *
 * @param string $form Form HTML.
 * @return string Modified form HTML.
 */
function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform pull-right" action="' . home_url( '/' ) . '" >
	 <div class="input-group">
	 <!-- <label class="screen-reader-text" for="s">' . __( 'Sök:' ) . '</label> -->
	 <input type="text" placeholder="Sök..." class="form-control" value="' . get_search_query() . '" name="s" id="s" />
	 <span class="input-group-btn">

	 </span>
	 </div>
	 </form>';

    return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );


add_action('wp_ajax_nopriv_post-like', 'post_like');
add_action('wp_ajax_post-like', 'post_like');

wp_enqueue_script('like_post', get_template_directory_uri().'/js/like.js', array('jquery'), '1.0', true );
wp_localize_script('like_post', 'ajax_var', array(
    'url' => admin_url('admin-ajax.php'),
    'nonce' => wp_create_nonce('ajax-nonce')
));
function post_like()
{
    // Check for nonce security
    $nonce = $_POST['nonce'];

    if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
        die ( 'Busted!');

    if(isset($_POST['post_like']))
    {
        // Retrieve user IP address
        $ip = $_SERVER['REMOTE_ADDR'];
        $post_id = $_POST['post_id'];

        // Get voters'IPs for the current post
        $meta_IP = get_post_meta($post_id, "voted_IP");
        $voted_IP = $meta_IP[0];

        if(!is_array($voted_IP))
            $voted_IP = array();

        // Get votes count for the current post
        $meta_count = get_post_meta($post_id, "votes_count", true);

        // Use has already voted ?
        if(!hasAlreadyVoted($post_id))
        {
            $voted_IP[$ip] = time();

            // Save IP and increase votes count
            update_post_meta($post_id, "voted_IP", $voted_IP);
            update_post_meta($post_id, "votes_count", ++$meta_count);

            // Display count (ie jQuery return value)
            echo $meta_count;
        }
        else
            echo "already";
    }
    exit;
}

$timebeforerevote = 120; // = 2 hours

function hasAlreadyVoted($post_id)
{
    global $timebeforerevote;

    // Retrieve post votes IPs
    $meta_IP = get_post_meta($post_id, "voted_IP");
    $voted_IP = $meta_IP[0];

    if(!is_array($voted_IP))
        $voted_IP = array();

    // Retrieve current user IP
    $ip = $_SERVER['REMOTE_ADDR'];

    // If user has already voted
    if(in_array($ip, array_keys($voted_IP)))
    {
        $time = $voted_IP[$ip];
        $now = time();

        // Compare between current time and vote time
        if(round(($now - $time) / 60) > $timebeforerevote)
            return false;

        return true;
    }

    return false;
}


function getPostLikeLink($post_id)
{
    $themename = "kaffe";

    $vote_count = get_post_meta($post_id, "votes_count", true);

    $output = '<p class="post-like">';
    if(hasAlreadyVoted($post_id))
        $output .= ' <span title="'.__('I like this article', $themename).'" class="like alreadyvoted"></span>';
    else
        $output .= '<a href="#" data-post_id="'.$post_id.'">

                    <span  title="'.__('I like this article', $themename).'"class="qtip like"><i class="fa fa-coffee" aria-hidden="true"></i></span>
                	</a>';
    $output .= '<i class="fa fa-coffee" aria-hidden="true"></i><span class="count">'.$vote_count.'</span></p>';

    return $output;
}







/*
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template scripts for this theme.
 */
require get_template_directory() . '/inc/template-scripts.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
 * Load Titan Framework plugin checker
 */
require get_template_directory() . '/titan-framework-checker.php';

/**
 * Load Titan Framework options
 */
require get_template_directory() . '/titan-options.php';
