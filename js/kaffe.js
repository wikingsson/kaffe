$.material.init();
$('#toggle').click(function() {
   $(this).toggleClass('active');
   $('#overlay').toggleClass('open');
  });

  jQuery(document).ready(function() {
      $('#list').click(function(event){event.preventDefault();$('#categories-list .item').addClass('list-group-item');});
      $('#grid').click(function(event){event.preventDefault();$('#categories-list .item').removeClass('list-group-item');$('#categories-list .item').addClass('grid-group-item');});
  });


  jQuery(document).ready(function($){
  	var news_position = -1;
  	var loading_news = false;

  	$('.kaffe-news-load-more').click(function() {
  		if (!loading_news) {
  			loading_news = true;
  			$('#escapingBallG').show();

  			var count = $(this).data("count");
  			if (news_position == -1) news_position = count;

  			var data = {
  				'action': 'kaffe_get_more_news',
  				'offset': news_position,
  				'count': count,
  				'posttype': $(this).data("posttype"),
  				'category': $(this).data("category"),
  			};
  			jQuery.post(ajax_object.ajax_url, data, function(response) {
  				loading_news = false;
  				if (response.length == 0) {
  					$('.kaffe-news-load-more').fadeOut();
  				} else {
  					news_position += count;
  					$('#kaffe-news-archive').append(response);
  				}
  				$('#escapingBallG').hide();
  			});
  		}
  	});
  });
