<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kaffe
 */

?>
<div class="container-single">
	<div class="col-xs-12 single-post gray">

		<?php the_title( '<h1>', '</h1>' ); ?>

		<?php the_content(); ?>


	</div>
</div>