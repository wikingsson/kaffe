<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kaffe
 */

?>

<div class="item col-xs-12 col-sm-6 col-lg-4">
   <div class='thumbnail'>
        <div class="card-image">
           <img class="group list-group-image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() );?>">
        </div>
        <div class="caption">
            <?php the_title( '<h2 class="group inner list-group-item-heading">', '</h2>' ); ?>
            <p class="group inner list-group-item-text"><?php echo wp_trim_words($post->post_content, 20, "...") ?></p>
            <a class="avsnittsguide hvr-underline-from-center" href="<?php the_permalink() ?>">Lyssna på Avsnittet</a>
        </div>
   </div>
</div>
