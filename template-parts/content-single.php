<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kaffe
 */

?>



<div class="container-single">

<div class="col-xs-12 single-post gray">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

      <?php echo do_shortcode( '[podcast_episode episode="' . get_the_ID() . '" content="player"]' ); ?>


      <?php the_content(); ?>

      <div class="pod-social-single pull-right">
         <a href="#" class="social-media facebook"><i class="fa fa-facebook fa-lg"></i></a>
         <a href="#" class="social-media twitter"><i class="fa fa-twitter fa-lg"></i></a>
         <a href="#" class="social-media youtube"><i class="fa fa-youtube fa-lg"></i></a>
         <a href="#" class="social-media itunes"><i class="fa fa-music fa-lg"></i></a>
      </div>

		<div class="row">
		</div>
			<div class="post-navigation">
				 <?php previous_post_link(); ?>
				 <div style="float: right"><?php next_post_link(); ?></div>
			</div>


</div>
</div>
