<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kaffe
 */

?>
<?php

$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );

 ?>




 <div class="col-xs-12 pod-item card-panel grey lighten-5">
    <div class="col-xs-12 col-md-5 pod-image">
      <img src="<?php echo $src[0]; ?>" alt="" class="responsive-img kaffe-colors">
      <div class="pod-social">
         <?php echo do_shortcode( '[podcast_episode episode="' . get_the_ID() . '" content="player"]' ); ?>
      </div>
    </div>

   <div class="col-xs-12 col-md-7 pod-content">
      <h1><?php the_title(); ?></h1>
      <?php //echo getPostLikeLink(get_the_ID());?>
      <p><?php echo wp_trim_words( get_the_content(), 50, '...' ); ?></p>
      <div class="pod-bottom">
         <?php echo do_shortcode( '[podcast_episode episode="' . get_the_ID() . '" content="player"]' ); ?>
         <a href="<?php the_permalink() ?>" class="social-media facebook"><i class="fa fa-facebook fa-lg"></i></a>
         <ul>
            <li><a href="#" class="social-media twitter"><i class="fa fa-twitter fa-lg"></i></a>
               <ul>
                  <li class="sub-twitter"><a href="#" class="social-media twitter"><i class="fa fa-twitter fa-lg"></i></a></li>
                  <li class="sub-twitter"><a href="#" class="social-media twitter"><i class="fa fa-twitter fa-lg"></i></a></li>
                  <li class="sub-twitter"><a href="#" class="social-media twitter"><i class="fa fa-twitter fa-lg"></i></a></li>
               </ul>
            </li>
         </ul>
         <a href="" class="social-media youtube"><i class="fa fa-youtube fa-lg"></i></a>
         <a href="" class="social-media itunes"><i class="fa fa-music fa-lg"></i></a>
         <a href="" class="social-media instagram"><i class="fa fa-instagram fa-lg"></i></a>
         <a href="<?php the_permalink() ?>" class="avsnittsguide hvr-underline-from-center">Avsnittsguide</a>
      </div>
   </div>
</div>
