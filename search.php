<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Kaffe
 */

get_header( 'single' ) ?>


<div class="container">
	<div class="row">
		<div class="col-xs-12 single-post gray">
			<div class="search-header">
				<h3 class="pull-left"><?php printf( esc_html__( 'Search Results for: %s', 'kaffe' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
				<?php get_search_form(); ?>
			</div>
		<?php
		if ( have_posts() ) : ?>


			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/archive', 'search' );

			endwhile;



		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
	</div>
</div>
</div>

<?php
get_sidebar();
get_footer();
