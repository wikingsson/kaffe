<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kaffe
 */

get_header(); ?>

<?php
		$titan = TitanFramework::getInstance( 'kaffe' );
		$imageID = $titan->getOption( 'kaffe_info_img' );
		// The value may be a URL to the image (for the default parameter)
		// or an attachment ID to the selected image.
		$imageSrc = $imageID; // For the default value
		if ( is_numeric( $imageID ) ) {
		   $imageAttachment = wp_get_attachment_image_src( $imageID, $size = 'full' );
		   $imageSrc = $imageAttachment[0];
		}

	?>


<div class="row">
	<div class="col-xs-12 om-content">
		<div class="col-xs-12 col-md-5 pull-right om-image">
			<img class="circle" src='<?php echo esc_url( $imageSrc ); ?>' />
		</div>
		<div class="col-xs-12 col-md-7">
			<div class="om-text">
				<h1>Tre grabbar. En timme. Utan gränser.</h1>
				<p><?php echo ( $titan->getOption( 'kaffe_info_text' ) ); ?></p>
				<?php
					if(empty($titan->getOption( 'kaffe_info_button_text' ) ) ) {
						  # only $link empty
					 } else {
						  ?><a href="#" class="btn-large kaffe-colors"><?php echo ( $titan->getOption( 'kaffe_info_button_text' ) ); ?></a><?
					 }
				 ?>


			</div>
		</div>
	</div>
</div>

<div class="section">
	<div class="row">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;


		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
	</div>
</div>

<?php
get_footer();
