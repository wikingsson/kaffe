

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kaffe
 *
 * Template Name: Top 10

 */

get_header( 'single' ) ?>

<div class="container">
	<div class="row">



			<?php
         global $query_string; // required
         $posts = query_posts('meta_key=votes_count&orderby=meta_value_num&order=DESC&posts_per_page=10');
         $vote_count = get_post_meta($post_id, "votes_count", true);
         ?>

         <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


         		<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h1>
               <i class="fa fa-coffee" aria-hidden="true"></i><span class="count"><?php echo $vote_count ?></span></p>'


         <?php endwhile; ?>





         <?php endif; ?>


         <?php wp_reset_query(); // reset the query ?>

         <?php echo do_shortcode( "[dot_recommended_posts post_type='post' container='div' number='10']" ); ?>


		</div>
	</div>

<?php

get_footer();
