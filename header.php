<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kaffe
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<?php

$titan = TitanFramework::getInstance( 'kaffe' );
$imageID = $titan->getOption( 'kaffe_logo_img' );
// The value may be a URL to the image (for the default parameter)
// or an attachment ID to the selected image.
$logoSrc = $imageID; // For the default value
if ( is_numeric( $imageID ) ) {
	$imageAttachment = wp_get_attachment_image_src( $imageID, $size = 'full' );
	$logoSrc = $imageAttachment[0];
}

 ?>

<body>

	<div class="button_container" id="toggle">
	  <span class="top"></span>
	  <span class="middle"></span>
	  <span class="bottom"></span>
	</div>

	<div class="overlay" id="overlay">
	  <nav class="overlay-menu">
	    <ul>
			 <?php wp_nav_menu(); ?>
	    </ul>
	  </nav>
	</div>



	<div class="section" id="index-banner">
		<div class="intro absolute-center">
			<a class="hvr-underline-from-center" href="<?php echo get_home_url(); ?>">
				<h1 class="header center"><?php echo ( $titan->getOption( 'kaffe_header_text' ) ); ?></h1>
			</a>
			<?php
				if(empty($titan->getOption( 'kaffe_logo_img' ) ) ) {
					  # only $link empty
				 } else {
					  ?><img class="logo" src='<?php echo esc_url( $logoSrc ); ?>' alt="<?php echo( get_bloginfo( 'title' ) ); ?>" /><?
				 }
			 ?>

			<h5><?php echo ( $titan->getOption( 'kaffe_sub_text' ) ); ?></h5>
			<?php
				if(empty($titan->getOption( 'kaffe_header_button_text' ) ) ) {
					  # only $link empty
				 } else {
					  ?><a href="<?php echo ( $titan->getOption( 'kaffe_header_button_page' ) ); ?>" class="btn-large kaffe-colors"><?php echo ( $titan->getOption( 'kaffe_header_button_text' ) ); ?></a><?
				 }
			 ?>




		</div>
	</div>
