<?php
/**
 * Add all the scripts here.
 */

/* Add bootstrap support*/




function theme_add_custom() {
   wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
   wp_enqueue_style( 'material', get_template_directory_uri() . '/css/bootstrap-material-design.css' );
   wp_enqueue_style( 'ripple', get_template_directory_uri() . '/css/ripples.min.css' );
   wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.0.0', true );
   wp_enqueue_script( 'material-js', get_template_directory_uri() . '/js/material.min.js', array(), '3.1.0', true );
   wp_enqueue_script( 'ripples-js', get_template_directory_uri() . '/js/ripples.min.js', array(), '3.0.2', true );

}

add_action( 'wp_enqueue_scripts', 'theme_add_custom' );

function register_global_template_scripts() {
    wp_enqueue_style( 'default-style', get_stylesheet_uri(), array(), '1.0.0' );
    wp_register_script('kaffe-js', get_template_directory_uri().'/js/kaffe.js', array(), '3.0.0', true );
    wp_enqueue_script('kaffe-js');
    wp_localize_script('kaffe-js', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action( 'wp_enqueue_scripts', 'register_global_template_scripts' );


//enqueues our external font awesome stylesheet
function enqueue_our_required_stylesheets(){
    wp_enqueue_style('font', '//fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array(), '3.0.0', true );
}
add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');



//jquery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['88'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js", true, null);
    wp_enqueue_script('jquery');
}


?>
