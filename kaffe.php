<?php
/*
Template Name: Arkiv
*/

$kaffe_count = 6;
get_header( 'single' ); ?>

<div class="container">
	<div class="row">
		<div class="container-single">
		<div class="col-xs-12 arkiv gray">
			<div class="search-header">
				<?php the_title( '<h1 class="pull-left">', '</h1>' ); ?>
				<?php get_search_form(); ?>
			</div>

			<?php if (have_posts()) : the_post(); ?>

				<div id="kaffe-news-archive" class="row list-group">
					<?php kaffe_get_news(0, $kaffe_count, 8, 'post'); ?>
				</div>
			<?php endif; ?>
			<div class="navigation">
				<div class="kaffe-news-load-more" data-posttype="post" data-category="8" data-count="<?php echo($kaffe_count); ?>">
					<p>Visa mer</p>
					<div id="escapingBallG">
						<div id="escapingBall_1" class="escapingBallG"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>












<?php get_footer(); ?>
